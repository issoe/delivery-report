package com.report.delivery;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.SpringApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class DeliveryApplication{
	public static void main(String[] args) {
		SpringApplication.run(DeliveryApplication.class, args);
	}
}
