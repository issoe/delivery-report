// function: convert entities received from repo to model report
package com.report.delivery.services;

import com.report.delivery.models.ResponseModel;
import org.springframework.http.ResponseEntity;

public interface ExamsService {
    public ResponseEntity<ResponseModel> getExams();
}
