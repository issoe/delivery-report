package com.report.delivery.entities;

import javax.persistence.*;

@Entity
@Table(name = "class")
public class Class {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "classcode")
    private String classcode;

    @Column(name = "size")
    private Integer size;

    @Column(name = "skill_id")
    private Integer skillId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private CourseStatus status;

    @Column(name = "fsu")
    private String fsu;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSkillId() {
        return skillId;
    }

    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }

    public CourseStatus getStatus() {
        return status;
    }

    public void setStatus(CourseStatus status) {
        this.status = status;
    }

    public String getFsu() {
        return fsu;
    }

    public void setFsu(String fsu) {
        this.fsu = fsu;
    }

}