package com.report.delivery.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ResponseModel {
    private String status;
    private String message;
    private Object data;

    public ResponseModel(String status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}